# Visualizing divergence


This challenge is around visualizing genetic divergence of HIV from the transmitted virus, over time.


There are many ways to compute a distances between two strings. One way to calculate genetic distance/divergence is to compute the number of nucleotide changes (distance) between two sequences (aka [Hamming distance](https://en.wikipedia.org/wiki/Hamming_distance)/[Edit distance](https://en.wikipedia.org/wiki/Edit_distance))
ie: The Hamming distance for the following two sequences is 2

- Sequence 1: ATCGATCGTG
- Sequence 2: ATC​**C**​AT​**G**​GTG


This distance is often normalized for sequence length and expressed as a percentage (20% in this case). 


You will receive two files in .fasta format. 
Each sequence in this format is made up of a two part block:

- a greater than sign (>) followed by a string/sequence identifier and a newline character
- the sequence (ATCGATCG) followed by a newline character


There can be many such blocks in a .fasta file. The above sequences in fasta format would be:

\>Sequence 1

ATCGATCG

\>Sequence 2

ATCCATGG


In our group we encode information into the sequence id as below:
CAP177_2000_004wpi_C1C2_002_0.075


Separated by underscores, this contains the following information:

- Participant identifier (CAP177)
- Visit code (2000)
- Weeks post infection (004wpi)
- Gene Region (C1C2)
- Unique numeric tag (002)
- Relative frequency of this sequence at that time point (0.075)


We would like you to:

1. Calculate the Hamming distance for each sequence to a given reference sequence (the initial transmitted virus) and store this output in a CSV file.
2. Produce a visualization of the divergence over time (default plot aesthetics will suffice for this example).


You may find the following python library useful, but feel free to use other libraries if you prefer:
[smallBixTools](https://pypi.org/project/smallBixTools/)

`pip install smallBixTools`

Then you can:
```python
from smallBixTools import smallBixTools as st
st.fasta_to_dct(“/path/to/file.fasta”)
```

Reading in DNA sequence files (fasta files) into a dictionary: `st.fasta_to_dct`
Computing distance between two sequences: `st.hamdist`


Work however you are most comfortable - Jupyter notebooks, docs, imports, etc… and share back to us what you do in whatever manner you feel suits best.


What we have asked above is one exploration path for this data. You are welcome to additionally explore this data from any angle, facet, grouping, anomaly, quirk, interest or categorization, etc… you may find. If you do this, you are welcome to send us what you have done - even if they are incomplete or dead ends. Some of the best research comes from simply playing with things, and noticing patterns.
